import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';
import Login from './Login';
import { MemoryRouter, BrowserRouter } from 'react-router-dom';

describe('<Login />', () => {
	window.alert = jest.fn();
	//button login have been exist
	it('button login have been exist', () => {
		const hideLayout = jest.fn();
		const LoginScreen = mount(
			<BrowserRouter>
				<Login isLogged={hideLayout} />
			</BrowserRouter>
		);
		expect(LoginScreen.find('input#login')).toExist();
	});

	//button login have been disable
	it('button login have been disable', () => {
		const hideLayout = jest.fn();
		const LoginScreen = mount(
			<BrowserRouter>
				<Login isLogged={hideLayout} />
			</BrowserRouter>
		);
		expect(LoginScreen.find('input#login')).toBeDisabled();
	});

	//check value username input is right
	it('check value username input is right', () => {
		const hideLayout = jest.fn();
		const LoginScreen = mount(
			<MemoryRouter>
				<Login isLogged={hideLayout} />
			</MemoryRouter>
		);
		LoginScreen.find('input#username').simulate('change', {
			target: {
				id: 'username',
				value: 'Duy'
			}
		});
		const user = LoginScreen.find(Login).childAt(0).childAt(0).instance();
		expect(user.state.username).toEqual('Duy');
	});

	//check username input is disabled
	it('check username input is disabled', () => {
		const hideLayout = jest.fn();
		const LoginScreen = mount(
			<MemoryRouter>
				<Login isLogged={hideLayout} />
			</MemoryRouter>
		);
		const input = LoginScreen.find('input#username');
		expect(input).toBeDisabled();
	});

	it('check username input is exits', () => {
		const hideLayout = jest.fn();
		const LoginScreen = mount(
			<MemoryRouter>
				<Login isLogged={hideLayout} />
			</MemoryRouter>
		);
		const input = LoginScreen.find('input#username');
		expect(input).toExist();
	});

	//check value password input is right
	it('check value password input is right', () => {
		const hideLayout = jest.fn();
		const LoginScreen = mount(
			<MemoryRouter>
				<Login isLogged={hideLayout} />
			</MemoryRouter>
		);
		LoginScreen.find('input#password').simulate('change', {
			target: {
				id: 'password',
				value: '123456'
			}
		});
		const user = LoginScreen.find(Login).childAt(0).childAt(0).instance();
		expect(user.state.password).toEqual('123456');
	});

	//check password input is disabled
	it('check password input is disabled', () => {
		const hideLayout = jest.fn();
		const LoginScreen = mount(
			<MemoryRouter>
				<Login isLogged={hideLayout} />
			</MemoryRouter>
		);
		const input = LoginScreen.find('input#password');
		expect(input).toBeDisabled();
	});

	//check password input is exits
	it('check password input is exits', () => {
		const hideLayout = jest.fn();
		const LoginScreen = mount(
			<MemoryRouter>
				<Login isLogged={hideLayout} />
			</MemoryRouter>
		);
		const input = LoginScreen.find('input#password');
		expect(input).toExist();
	});

	//button login to submit login form
	it('button login to submit call', () => {
		const hideLayout = jest.fn();
		const spy = sinon.spy();
		const LoginScreen = mount(
			<MemoryRouter>
				<Login isLogged={hideLayout} handleSubmit={spy} />
			</MemoryRouter>
		);
		// const LoginForm = LoginScreen.find(Login).childAt(0).childAt(0).instance();
		// const spyOn = sinon.spy(LoginForm, "handleSubmit");
		LoginScreen.find('input#login').simulate('submit');
		setTimeout(() => {
			expect(spy.called).toEqual(true);
		}, 0);
	});

	//check submit info right
	it('check submit info right', () => {
		const hideLayout = jest.fn();
		const submit = sinon.spy();
		const LoginScreen = mount(
			<MemoryRouter>
				<Login isLogged={hideLayout} handleSubmit={submit} />
			</MemoryRouter>
		);
		LoginScreen.find('input#password').simulate('change', {
			target: {
				id: 'password',
				value: '123456'
			}
		});
		LoginScreen.find('input#username').simulate('change', {
			target: {
				id: 'username',
				value: 'duy'
			}
		});
		LoginScreen.find('input#login').simulate('submit');
		const user = LoginScreen.find(Login).childAt(0).childAt(0).instance();
		expect(user.state.error).toEqual('');
	});

	//check submit user name empty
	it('check submit username empty', () => {
		const hideLayout = jest.fn();
		const submit = sinon.spy();
		const LoginScreen = mount(
			<MemoryRouter>
				<Login isLogged={hideLayout} handleSubmit={submit} />
			</MemoryRouter>
		);
		LoginScreen.find('input#password').simulate('change', {
			target: {
				id: 'password',
				value: '123456'
			}
		});
		LoginScreen.find('input#login').simulate('submit');
		const user = LoginScreen.find(Login).childAt(0).childAt(0).instance();
		expect(user.state.error).toEqual('Bạn chưa nhập username');
	});

	//check submit info right
	it('check submit password empty', () => {
		const hideLayout = jest.fn();
		const submit = sinon.spy();
		const LoginScreen = mount(
			<MemoryRouter>
				<Login isLogged={hideLayout} handleSubmit={submit} />
			</MemoryRouter>
		);
		LoginScreen.find('input#username').simulate('change', {
			target: {
				id: 'username',
				value: 'duy'
			}
		});
		LoginScreen.find('input#login').simulate('submit');
		const user = LoginScreen.find(Login).childAt(0).childAt(0).instance();
		expect(user.state.error).toEqual('Bạn chưa nhập password');
	});
});
